package com.example.demo.service;

import com.example.demo.repository.dto.BookDTO;
import com.example.demo.repository.dto.CategoryDTO;

import java.util.List;

public interface BookService {
    BookDTO insert(BookDTO book);
    List<BookDTO> selectAll();
    List<BookDTO> selectOne(int id);
    List<BookDTO> searchByTitle(String title);
    BookDTO findOne(int id);
    boolean delete(int id);
    BookDTO update(BookDTO book);

}
