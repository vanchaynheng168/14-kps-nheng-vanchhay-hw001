package com.example.demo.service.service.imp;

import com.example.demo.repository.BookRepository;
import com.example.demo.repository.dto.BookDTO;
import com.example.demo.repository.dto.CategoryDTO;
import com.example.demo.service.BookService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    private BookRepository bookRepository;

    public BookServiceImp(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    @Override
    public BookDTO insert(BookDTO book) {
        boolean isInserted = bookRepository.insert(book);
        if(isInserted)
            return book;
        else
            return null;
    }

    @Override
    public List<BookDTO> selectAll() {
        return bookRepository.selectAll();
    }

    @Override
    public BookDTO findOne(int id) {
       return bookRepository.findOne(id);
    }

    @Override
    public boolean delete(int id) {
        return bookRepository.delete(id);
    }

    @Override
    public BookDTO update( BookDTO book) {
        boolean isUpdate = bookRepository.update(book);
        if(isUpdate)
            return book;
        else
            return null;
    }

    @Override
    public List<BookDTO> selectOne(int id) {
        return bookRepository.selectOne(id);
    }

    @Override
    public List<BookDTO> searchByTitle(String title) {
        return bookRepository.searchByTitle(title);
    }
}
