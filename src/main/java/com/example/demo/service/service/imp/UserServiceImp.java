package com.example.demo.service.service.imp;

import com.example.demo.repository.UserRepository;
import com.example.demo.repository.dto.RoleDTO;
import com.example.demo.repository.dto.UserDTO;
import com.example.demo.rest.response.Messages;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserServiceImp implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO insert(UserDTO userDTO) {
        try {
            boolean isInserted = userRepository.insert(userDTO);
            if(isInserted){
                int id = userRepository.selectIdByUserId(userDTO.getUserId());
                userDTO.setId(id);
                for(RoleDTO role : userDTO.getRole()){
                    userRepository.createUserRoles(userDTO,role);
                }
            }
            return isInserted ? userDTO : null;

        }catch (Exception e){
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    Messages.Error.INSERT_FAILURE.getMessage()
            );
        }
    }
}
