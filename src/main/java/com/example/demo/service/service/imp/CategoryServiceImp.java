package com.example.demo.service.service.imp;

import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.dto.CategoryDTO;
import com.example.demo.service.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    private CategoryRepository categoryRepository;

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDTO insert(CategoryDTO category) {
        boolean isInserted = categoryRepository.insert(category);
        if(isInserted){
            return category;
        }
        else
            return null;
    }

    @Override
    public List<CategoryDTO> selectAll() {
        return categoryRepository.selectAll();
    }

    @Override
    public CategoryDTO findId(int id) {
        return categoryRepository.findID(id);
    }

    @Override
    public boolean delete(int id) {
        return categoryRepository.delete(id);
    }

    @Override
    public CategoryDTO update(CategoryDTO category) {
        boolean isUpdated = categoryRepository.update(category);
        if(isUpdated){
            return category;
        }
        else return null;
    }

}
