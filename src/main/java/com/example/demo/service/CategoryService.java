package com.example.demo.service;

import com.example.demo.repository.dto.CategoryDTO;

import java.util.List;

public interface CategoryService {
    List<CategoryDTO> selectAll();
    CategoryDTO insert(CategoryDTO category);
    CategoryDTO update(CategoryDTO category);
    CategoryDTO findId(int id);
    boolean delete(int id);

}
