package com.example.demo.service;

import com.example.demo.repository.dto.UserDTO;

public interface UserService {
    UserDTO insert(UserDTO userDTO);
}
