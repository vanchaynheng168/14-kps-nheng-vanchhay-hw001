package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/users").permitAll()
                .antMatchers(HttpMethod.POST,"/books").permitAll()
                .antMatchers(HttpMethod.GET,"/books").permitAll()
                .antMatchers(HttpMethod.GET,"/books/{id}").permitAll()
                .antMatchers(HttpMethod.PUT,"/books/{id}").permitAll()
                .antMatchers(HttpMethod.DELETE,"/books/{id}").permitAll()
                .antMatchers(HttpMethod.POST,"/category").permitAll()
                .antMatchers(HttpMethod.GET,"/category").permitAll()
                .antMatchers(HttpMethod.PUT,"/category/{id}").permitAll()
                .antMatchers(HttpMethod.DELETE,"/category/{id}").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }
    public void configure(AuthenticationManagerBuilder auth) throws Exception{
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password(passwordEncoder().encode("123abc"))
                .authorities("ROLE_USER");
    }
}
