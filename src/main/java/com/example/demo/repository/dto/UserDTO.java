package com.example.demo.repository.dto;

import java.util.List;

public class UserDTO {
    private int id;
    private String userId;
    private String username;
    private String password;

    private List<RoleDTO> role;

    public UserDTO(){}

    public UserDTO(int id, String userId, String username, String password, List<RoleDTO> role) {
        this.id = id;
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public List<RoleDTO> getRole() {
        return role;
    }

    public void setRole(List<RoleDTO> role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
