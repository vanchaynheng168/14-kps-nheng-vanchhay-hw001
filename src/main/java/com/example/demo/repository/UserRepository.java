package com.example.demo.repository;

import com.example.demo.repository.dto.RoleDTO;
import com.example.demo.repository.dto.UserDTO;
import com.example.demo.repository.provider.UserProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.postgresql.util.PSQLException;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository {

    @InsertProvider(type = UserProvider.class, method = "insertUserSql")
    boolean insert(UserDTO userDTO) throws PSQLException;
    @InsertProvider(type = UserProvider.class, method = "createUserRolesSql")
    boolean createUserRoles(UserDTO user, RoleDTO role) ;
    @InsertProvider(type = UserProvider.class, method = "selectIdByUserId")
    int selectIdByUserId(String userId);
}
