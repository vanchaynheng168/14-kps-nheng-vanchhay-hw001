package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

    public String insertUserSql(){
        return new SQL(){{
            //Define SQL
            INSERT_INTO("users");
            VALUES("user_id", "#{userId}");
            VALUES("username","#{username}");
            VALUES("password","#{password}");

        }}.toString();
    }
    public String createUserRolesSql(){
        return new SQL(){{
            INSERT_INTO("users_roles");
            VALUES("user_id", "#{user.id}");
            VALUES("role_id", "#{role.id}");
        }}.toString();
    }
    public String selectIdByUserId(){
        return new SQL(){{
            SELECT("id");
            FROM("users");
            WHERE("user_id = #{userId}");
        }}.toString();
    }

}
