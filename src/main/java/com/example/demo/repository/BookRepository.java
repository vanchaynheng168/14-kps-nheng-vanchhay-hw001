package com.example.demo.repository;

import com.example.demo.repository.dto.BookDTO;
import com.example.demo.repository.dto.CategoryDTO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    @Insert("INSERT INTO tb_books(title, author, description) VALUES (#{title}, #{author}, #{description})")
    boolean insert(BookDTO book);
    @Select("SELECT * FROM tb_books")
    List<BookDTO> selectAll();
    @Select("SELECT * FROM tb_books WHERE id=#{id}")
    List<BookDTO> selectOne(int id);

    @Select("select * from tb_categorys where title LIKE #{%title%}")
    List<BookDTO> searchByTitle(String title);

    @Select("SELECT id FROM tb_books WHERE id=#{id}")
    BookDTO findOne(int id);
    @Delete("DELETE FROM tb_books WHERE id=#{id}")
    boolean delete(int id);
    @Update("UPDATE tb_books set title=#{title}, author=#{author}, description= #{description} WHERE id=#{id}")
    boolean update(BookDTO book);
}
