package com.example.demo.repository;

import com.example.demo.repository.dto.BookDTO;
import com.example.demo.repository.dto.CategoryDTO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {


    @Insert("INSERT INTO tb_categorys (title) VALUES (#{title})")
    boolean insert(CategoryDTO category);
    @Select("SELECT * FROM tb_categorys")
    List<CategoryDTO> selectAll();
    @Select("SELECT id from tb_categorys where id =#{id}")
    CategoryDTO findID(int id);
    @Delete("DELETE FROM tb_categorys WHERE id=#{id}")
    boolean delete(int id);
    @Update("UPDATE tb_categorys set title = #{title}")
    boolean update(CategoryDTO category);

}
