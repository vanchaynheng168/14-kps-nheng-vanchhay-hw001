package com.example.demo.rest.restController;

import com.example.demo.repository.dto.UserDTO;
import com.example.demo.rest.request.UserRequestModel;
import com.example.demo.rest.response.BaseAPIResponse;
import com.example.demo.rest.response.Messages;
import com.example.demo.rest.response.UserRestAPI;
import com.example.demo.rest.utils.CommonUtils;
import com.example.demo.service.service.imp.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@RequestMapping("/api/v1")
public class UserRestController {

    private UserServiceImp userService;
    private CommonUtils commonUtils;

    private BCryptPasswordEncoder encoder;

//    @Autowired
//    public void setEncoder(BCryptPasswordEncoder encoder) {
//        this.encoder = encoder;
//    }

    @Autowired
    public UserRestController(UserServiceImp userService) {
        this.userService = userService;
    }

    @Autowired
    public void setCommonUtils(CommonUtils commonUtils) {
        this.commonUtils = commonUtils;
    }
    @PostMapping("/users")
    public ResponseEntity<BaseAPIResponse<UserRestAPI>> insert(
            @RequestBody UserRequestModel user
            ){

        System.out.println("Hello");
        BaseAPIResponse<UserRestAPI> response = new BaseAPIResponse<>();

        UserDTO userDTO = commonUtils.getMapper().map(user, UserDTO.class);  //UserDTO.class is class definition

        //generate user_id
        UUID uuid = UUID.randomUUID();
        userDTO.setUserId((uuid.toString()));
//        userDTO.setPassword();

        UserDTO insertedUser = userService.insert(userDTO);

        UserRestAPI userRestAPI = commonUtils.getMapper().map(insertedUser,UserRestAPI.class);

        response.setMessage(Messages.Success.INSERT_SUCCESS.getMessage());
        response.setData(userRestAPI);
        response.setStatus(HttpStatus.OK);
        response.setTime(commonUtils.getCurrentTime());
        return ResponseEntity.ok(response);
    }
}
