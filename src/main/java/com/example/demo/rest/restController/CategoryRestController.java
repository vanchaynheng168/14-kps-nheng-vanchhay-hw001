package com.example.demo.rest.restController;

import com.example.demo.repository.dto.CategoryDTO;
import com.example.demo.rest.request.CategoryRequestModel;
import com.example.demo.rest.response.BaseAPIResponse;
import com.example.demo.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class CategoryRestController {
    private CategoryService categoryService;
    @Autowired

    public CategoryRestController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/category")
    public ResponseEntity<BaseAPIResponse<List<CategoryRequestModel>>> select(){

        ModelMapper mapper = new ModelMapper();
        BaseAPIResponse<List<CategoryRequestModel>> response = new BaseAPIResponse<>();
        List<CategoryDTO> categoryDTOList = categoryService.selectAll();
        List<CategoryRequestModel> category = new ArrayList<>();

        for (CategoryDTO categoryDTO : categoryDTOList){
            category.add(mapper.map(categoryDTO, CategoryRequestModel.class));
        }
        response.setMessage("Data have found all books successfully");
        response.setData(category);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @PostMapping("/category")
    public ResponseEntity<BaseAPIResponse<CategoryRequestModel>> insert(
            @RequestBody CategoryRequestModel category){
        BaseAPIResponse<CategoryRequestModel> response = new BaseAPIResponse<>();
        //Validate

        ModelMapper mapper = new ModelMapper();
        CategoryDTO categoryDTO = mapper.map(category, CategoryDTO.class);
        CategoryDTO result = categoryService.insert(categoryDTO);

        CategoryRequestModel result2 = mapper.map(result, CategoryRequestModel.class);

        response.setMessage("You have saved successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return  ResponseEntity.ok(response);
    }
    @DeleteMapping("/category/{id}")  //request path variable
    public Map<String,Object> deleteBook(@PathVariable int id){
        Map<String,Object> result=new HashMap<>();
        CategoryDTO categoryDTO=categoryService.findId(id);
        if(categoryDTO==null){
            result.put("Message","Id Not Found Delete Unsuccessfully");
            result.put("Response Code",404);
        }else{
            categoryService.delete(id);
            result.put("Message","Deleted Successfully");
            result.put("Response Code",200);
        }
        return result;
    }
    @PutMapping("/category/{id}")
    public Map<String,Object> updateCategory(@PathVariable int id,@RequestBody CategoryDTO category){
        Map<String,Object> result=new HashMap<>();
        CategoryDTO categoryDTO=categoryService.findId(id);
        if(categoryDTO==null){
            result.put("Message","CODE Not Found Update Unsuccessfully");
            result.put("Response Code",404);
        }else{
            category.setId(id);
            categoryService.update(category);
            result.put("Message","Update Successfully");
            result.put("Response Code",200);
            result.put("DATA",category);
        }
        return result;
    }
}
