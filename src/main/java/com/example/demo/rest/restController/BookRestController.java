package com.example.demo.rest.restController;

import com.example.demo.repository.dto.BookDTO;
import com.example.demo.rest.request.BookRequestModel;
import com.example.demo.rest.response.BaseAPIResponse;
import com.example.demo.service.service.imp.BookServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")

public class BookRestController {

    private BookServiceImp bookService;

    @Autowired
    public BookRestController(BookServiceImp bookService) {
        this.bookService = bookService;
    }
    @GetMapping("/books")
    public ResponseEntity<BaseAPIResponse<List<BookRequestModel>>> select(){

        ModelMapper mapper = new ModelMapper();
        BaseAPIResponse<List<BookRequestModel>> response = new BaseAPIResponse<>();
        List<BookDTO> bookDTOList = bookService.selectAll();
        List<BookRequestModel> books = new ArrayList<>();

        for (BookDTO bookDTO : bookDTOList){
            books.add(mapper.map(bookDTO, BookRequestModel.class));
        }
        response.setMessage("Data have found all books successfully");
        response.setData(books);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @GetMapping("/books/{id}")
    public Map<String,Object> getOne(@PathVariable int id){
        System.out.println("/books/{id} = "+id);
        Map<String,Object> result=new HashMap<>();
        List<BookDTO> book=bookService.selectOne(id);
        System.out.println(book);
        if(book.isEmpty()){
            result.put("Message","No data in database");
        }else{
            result.put("Message","get successfully");
        }
        result.put("DATA",book);
        return result;
    }
    @GetMapping("/books/find")
    public Map<String,Object> searchByTitle(@RequestParam String title){
        Map<String,Object> result=new HashMap<>();
        List<BookDTO> book=bookService.searchByTitle(title);
        System.out.println(title);
        System.out.println(book);
        if(book.isEmpty()){
            result.put("Message","No data");
        }else{
            result.put("Message","get successfully");
        }
        result.put("DATA",book);
        return result;
    }
    @PostMapping("/books")
    public ResponseEntity<BaseAPIResponse<BookRequestModel>> insert(
            @RequestBody BookRequestModel book){
        BaseAPIResponse<BookRequestModel> response = new BaseAPIResponse<>();
        //Validate

        ModelMapper mapper = new ModelMapper();
        BookDTO bookDTO = mapper.map(book, BookDTO.class);
        BookDTO result = bookService.insert(bookDTO);

        BookRequestModel result2 = mapper.map(result, BookRequestModel.class);

        response.setMessage("You have saved successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return  ResponseEntity.ok(response);
    }

    @DeleteMapping("/books/{id}")  //request path variable
    public Map<String,Object> deleteBook(@PathVariable int id){
        Map<String,Object> result=new HashMap<>();
        BookDTO bookDTO=bookService.findOne(id);
        if(bookDTO==null){
            result.put("Message","Id Not Found Delete Unsuccessfully");
            result.put("Response Code",404);
        }else{
            bookService.delete(id);
            result.put("Message","Deleted Successfully");
            result.put("Response Code",200);
        }
        return result;
    }

    @PutMapping("/books/{id}")
    public Map<String,Object> updateBook(@PathVariable int id,@RequestBody BookDTO book){
        Map<String,Object> result=new HashMap<>();
        BookDTO bookDTO=bookService.findOne(id);
        if(bookDTO==null){
            result.put("Message","CODE Not Found Update Unsuccessfully");
            result.put("Response Code",404);
        }else{
            book.setId(id);
            bookService.update(book);
            result.put("Message","Update Successfully");
            result.put("Response Code",200);
            result.put("DATA",book);
        }
        return result;
    }

}
