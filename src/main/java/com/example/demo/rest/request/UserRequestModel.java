package com.example.demo.rest.request;

import com.example.demo.repository.dto.RoleDTO;

import java.util.List;

public class UserRequestModel {
    private String username;
    private String password;

    private List<RoleDTO> roles;

    public UserRequestModel(){}

    public UserRequestModel(String username, String password, List<RoleDTO> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    public List<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleDTO> roles) {
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserRequestModel{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                '}';
    }
}
